var app = angular.module('funToLearn');

app.directive('ngModal', function () {
    return {
        scope: {
            headTitle: '@',
            show: "="
        },
        replace: true,
        transclude: true,
        templateUrl: 'Scripts/Application/NgModal/modal.html',
        link: function (scope, element, attrs) {

            var modal = element;

            scope.$watch('show', function () {
                if (scope.show > 0)
                    $(modal).modal('show');
                else
                    $(modal).modal('hide');
            });

            element.find("#close").click(function () {
                $(modal).modal('hide');
            });

        }
    }
});
